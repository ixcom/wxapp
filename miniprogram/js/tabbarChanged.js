module.exports = {
    onTabbarChange: function (event) {
        let that = this;
        let { detail } = event;
        if (detail == that.data.tap) {
            return false;
        }
        switch (detail) {
            case 0:
                wx.redirectTo({
                    url: '/pages/index/index',
                })
                break;
            case 1:
                wx.redirectTo({
                    url: '/pages/apps/index/index',
                })
                break;
            case 2:
                wx.redirectTo({
                    url: '/pages/message/index/index',
                })
                break;
            case 3:
                wx.redirectTo({
                    url: '/pages/userCenter/index/index',
                })
                break;
        }
    }
}