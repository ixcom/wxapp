// pages/userCenter/index/index.js
const app = getApp()

Page({
	mixins: [require('./../../../js/tabbarChanged')],
	/**
	 * 页面的初始数据
	 */
	data: {
		// tabbar 当前选中项
		tabbarStatus: {
			active: 3
		}
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		this.setData({
			isAdmin: app.globalData.isAdmin || false
		})
	},

	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function () {

	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {
		var that = this;
		app.checkUserLogin().then((res)=>{
			// console.log("usercenter on show", res)
			that.setData({
				user: res[0]
			})
		}).catch((err)=>{
			console.log("usercenter onshow catch", err)
		}) 
	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide: function () {

	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function () {

	},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh: function () {

	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {

	},

	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage: function () {

	},

	admin() {
		wx.navigateTo({
			url: '../../adminConsole/index/index',
		})
	}
})