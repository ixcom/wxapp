// pages/userCenter/account/complete/complete.js
Page({

    /**
     * 页面的初始数据
     */
    data: {
        company: ['江苏三仪', '动物营养', '巨托科技', '昌浩水产', '国托检测', '科研中心', '采购中心', '企划中心', '物流中心', '营销中心', '其他'],
        company_text: "请选择"
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        var that = this;
        let pages = getCurrentPages();
        let currPage = pages[pages.length - 1];
        // let info = currPage.data;

        // console.log(currPage.);
    },
    // onRealNameChange({ detail }) {
    //     this.setData({
    //         realname: detail
    //     })
    // },
    checkRealNameStatus() {
        let chs = /^[\u4e00-\u9fa5]{2,4}$/;
        let realname = this.data.realname;
        var that = this;
        if (!chs.test(realname)) {
            wx.showToast({
                title: '非中国姓名',
                icon: 'error',
                success() {
                    that.setData({
                        realname: ""
                    })
                }
            })
        }
    },
    // onMobileChange({ detail }) { this.setData({ mobile: detail }) },
    checkMobileStatus() {
        let phone = /^1(3[0-9]|4[5,7]|5[0,1,2,3,5,6,7,8,9]|6[2,5,6,7]|7[0,1,7,8]|8[0-9]|9[1,8,9])\d{8}$/;
        let mobile = this.data.mobile;
        var that = this;
        if (!phone.test(mobile)) {
            wx.showToast({
                title: '号码错误',
                icon: 'error',
                success() {
                    that.setData({
                        mobile: ""
                    })
                }
            })
        }
    },
    bindPickerChange(e) {
        this.setData({
            company_text: this.data.company[e.detail.value]
        })
    },

    confirm() {
        wx.showLoading({
            title: '提交注册',
        })
        var that = this;
        var realname = that.data.realname,
            mobile = that.data.mobile,
            company = that.data.company_text;
        if (realname == "" || mobile == "") {
            app.showToast('请认真填写', 2);
            return false;
        }
        // 开始补全信息
        wx.cloud.callFunction({
            name: 'user',
            data: {
                type: "completeUserInfo",
                realname: realname,
                mobile: mobile,
                team: company
            }
        }).then((res)=>{
            console.log(res)
            if (res.result.errMsg == "collection.update:ok") {
                setTimeout(()=>{wx.hideLoading()},500)

                wx.redirectTo({
                  url: '../../index/index',
                })

                // wx.navigateTo({
                //   url: 'url',
                // })
            }
        }).catch((err)=>{console.log(err)});
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {
        var that = this
        // wx.cloud.callFunction({
        //     name: 'user',
        //     data: {
        //         type: "getUser"
        //     }
        // }).then((user) => {
        //     console.log(user)
        //     that.setData({
        //         user: user.result.data[0]
        //     })
        // })
    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})