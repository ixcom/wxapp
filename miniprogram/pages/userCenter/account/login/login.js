// pages/userCenter/account/login/login.js
Page({

    /**
     * 页面的初始数据
     */
    data: {

    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {

    },

    /**
     * 微信授权登录
     * @param {*} ev 
     */
    login: function ({detail}) {
        var that = this;
        wx.showLoading({
          title: '登录中',
        });
        var { userInfo } = detail;
        wx.cloud.callFunction({
            name: 'user',
            data: {
                type: 'userAuthLogin',
                userinfo: userInfo
            }
        }).then((user) => {
            console.log(user.result)
            if (user.result.code === 1001) {
                // 关闭本页面，跳转至完善资料
                wx.redirectTo({
                  url: '../complete/complete',
                })
            } else if (user.result.code === 1000) {
                // 登录成功, 回到初始页面
                wx.redirectTo({
                  url: '../../../index/index',
                })
            }
        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})