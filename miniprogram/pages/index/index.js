//index.js
Page({
	mixins: [require('./../../js/tabbarChanged')],
	/**
	 * 页面的初始数据
	 */
	data: {
		// tabbar 当前选中项
		tabbarStatus: {
			active: 0
		}
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		var that = this
		// wx.getSystemInfo({
		// 	success: (result) => {
		// 		console.log(result.statusBarHeight)
		// 		that.setData({
		// 			topbar: result.statusBarHeight,
		// 			topheight: result.statusBarHeight + 40
		// 		})
		// 	},
		// })
	},

	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function () {
		// var that = this
		// wx.cloud.callFunction({
		// 	name: 'user',
		// 	data: {
		// 		type: "getUser"
		// 	}
		// }).then((res) => {
		// 	console.log("userinfo: ", res.result)
		// 	that.setData({
		// 		user: res.result.data[0]
		// 	})
		// })
	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {



	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide: function () {

	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function () {

	},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh: function () {

	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {

	},

	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage: function () {

	}
})