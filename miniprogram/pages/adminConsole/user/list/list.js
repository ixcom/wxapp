const admin = require("../../../../js/admin")

// pages/adminConsole/user/list/list.js
Page({

    /**
     * 页面的初始数据
     */
    data: {
        option1: [
            { text: '全部用户', value: 0 },
            { text: '新款商品', value: 1 },
            { text: '活动商品', value: 2 },
        ],
          option2: [
            { text: '默认排序', value: 'a' },
            { text: '按注册时间', value: 'b' },
            { text: '按积分', value: 'c' },
        ],
        value1: 0,
        value2: 'a',
        userList: []
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        const that = this
        wx.showLoading()
        wx.cloud.callFunction({
            name: "admin",
            data: {
                type: "getUserList"
            }
        }).then(function (res) {
            console.log("admin Function: ", res.result.data);
            that.setData({
                userList: res.result.data
            })
            setTimeout(()=>{
                wx.hideLoading({
                  success: (res) => {},
                })
            })
        })
    },

    jump: function (e) {
        console.log(e.currentTarget.dataset.id)
        wx.navigateTo({
          url: '../detail/detail?id=' + e.currentTarget.dataset.id
        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})