
// pages/adminConsole/user/detail/detail.js
Page({

    /**
     * 页面的初始数据
     */
    data: {
        editUser: "",
        editNameShow: false,
        editMobileShow: false,
        editTeamShow: false,
        editCoinShow: false,
        fileList: [{
            url: 'https://img.yzcdn.cn/vant/leaf.jpg',
            name: '图片1',
        }]
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        wx.showLoading({
          title: '加载中',
        })
        if (!options.hasOwnProperty('id')) {
            wx.showToast({
                title: "参数错误",
                icon: "error",
                success: function (e) {
                    setTimeout(() => {
                        wx.navigateBack()
                    }, 1000)
                }
            })
        }
        this.refreshUserData(options.id)

        // wx.cloud.callFunction({
        //     name: "admin",
        //     data: {
        //         type: "getUserDetail",
        //         id: options.id
        //     }
        // }).then((res) => {
        //     console.log(res.result.data)
        //     this.setData({
        //         editUser: res.result.data
        //     })
        // }).catch((err) => { console.log(err) })

    },

    editName: function () {
        var that = this
        that.setData({
            realname: that.data.editUser.realName,
            editNameShow: true
        })
    },
    changeNickName(){
        var that = this
        if (that.data.realname != that.data.editUser.realName) {
            wx.cloud.callFunction({
                name: "admin",
                data: {
                    type: "changeUserField",
                    id: that.data.editUser._id,
                    field: 'realname',
                    content: that.data.realname
                }
            }).then((res)=>{
                console.log(res.result)
                if (res.result.errMsg == "document.update:ok") {
                    that.refreshUserData(that.data.editUser._id)
                    wx.showToast({
                      title: '修改完成',
                    })
                }
                that.hidePopup()
            })
        } else {

        }
    },
    editMobile: function () {
        wx.showToast({
          title: '禁止编辑用户手机',
          icon: "none"
        })
        // this.setData({
        //     editMobileShow: true
        // })
    },
    editTeam: function () {
        this.setData({
            editTeamShow: true
        })
    },
    editCoin: function () {
        this.setData({
            editCoinShow: true
        })
    },

    refreshUserData (id) {
        
        wx.cloud.callFunction({
            name: "admin",
            data: {
                type: "getUserDetail",
                id: id
            }
        }).then((res) => {
            // console.log(res.result)
            if (res.result.errMsg == "document.get:ok") {
                this.setData({
                    editUser: res.result.data
                })
                setTimeout(()=>{
                    wx.hideLoading({
                      success: (res) => {},
                    })
                }, 500)
            }
            
        }).catch((err) => { console.log(err) })
    },

    onClose() {
        var that = this
        wx.showToast({
            title: '取消本次操作',
            icon: 'error',
            success: function () {
                that.hidePopup()
            }
        })

    },

    hidePopup () {
        if (this.data.editNameShow) {
            this.setData({
                editNameShow: false
            })
        } else if (this.data.editMobileShow) {
            this.setData({
                editMobileShow: false
            })
        } else if (this.data.editTeamShow) {
            this.setData({
                editTeamShow: false
            })
        } else if (this.data.editCoinShow) {
            this.setData({
                editCoinShow: false
            })
        }
    },
    /**
     * 封禁用户
     */
    disable() {
        var that = this
        wx.cloud.callFunction({
            name: "admin",
            data: {
                type: "changeUserField",
                id: that.data.editUser._id,
                field: 'status',
                content: 2
            }
        }).then((res)=>{
            console.log(res.result)
            if (res.result.errMsg == "document.update:ok") {
                that.refreshUserData(that.data.editUser._id)
                wx.showToast({
                  title: '用户被封停',
                })
            }
            that.hidePopup()
        })
    },
    /**
     * 积分操作
     */
    changeCoin(){
        // 现判断积分是否有效
        var that = this;
        var num = /^(0|[1-9][0-9]*|-[1-9][0-9]*)$/;
        var coin = that.data.coin, remark = that.data.remark || "系统奖励";
        if (!num.test(coin)) {
            wx.showToast({
              title: '数值异常',
              icon: 'error'
            })
            return false;
        }
        wx.cloud.callFunction({
            name: "admin",
            data: {
                type: "changeUserField",
                id: that.data.editUser._id,
                field: 'coin',
                content: coin,
                remark: remark
            }
        }).then((res)=>{
            console.log(res.result)
            if (res.result.errMsg == "success1") {
                that.refreshUserData(that.data.editUser._id)
                wx.showToast({
                  title: '积分操作成功',
                })
            } else {
                wx.showToast({
                    title: '系统异常',
                })
            }
            that.hidePopup()
        })
        

    },


    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})