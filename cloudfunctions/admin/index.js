const getUserDetail = require('./GetUserDetail/index')
const getUserList = require('./GetUserList/index')
const changeUserField = require('./ChangeUserField/index')

const cloud = require('wx-server-sdk')

cloud.init({
	env: cloud.DYNAMIC_CURRENT_ENV
})

// 云函数入口函数
exports.main = async (event, context) => {
	switch (event.type) {
		case 'getUserDetail':
			return await getUserDetail.main(event, context)
		case 'getUserList':
			return await getUserList.main(event, context)
		case 'changeUserField':
			return await changeUserField.main(event, context)
	}
}