// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()

const checkAdmin = require('./checkAdmin/index')
const userAuthLogin = require('./userAuthLogin/index')
const getGroupList = require('./getGroupList/index')
const getUser = require('./getUser/index')
const completeUserInfo = require('./completeUserInfo/index')
// 云函数入口函数
exports.main = async (event, context) => {
    switch (event.type) {
        case 'checkAdmin':
            return await checkAdmin.main(event, context)
        case 'userAuthLogin':
            return await userAuthLogin.main(event, context)
        case 'getGroupList':
            return await getGroupList.main(event, context)
        case 'completeUserInfo':
            return await completeUserInfo.main(event, context)
        case 'getUser':
            return await getUser.main(event, context)
    }
}